package de.oss4home.bc.controller;

import java.util.List;

import de.oss4home.swagger.generate.api.NotFoundException;
import de.oss4home.swagger.generate.model.Device;
import de.oss4home.swagger.generate.model.Devices;
import de.oss4home.swagger.generate.model.UpdateDevice;
import de.oss4home.swagger.generate.model.Value;

public interface DevicesController {

    public void changeDeviceSettings(String deviceId, UpdateDevice setting) throws NotFoundException;

    public Device getDevices(String deviceId) throws NotFoundException;

    public Value getDeviceValue(String deviceId, String valueId) throws NotFoundException;

    public List<Value> getDeviceValues(String deviceId) throws NotFoundException;

    public Devices getDevices(String devicetype, String sytemtype) throws NotFoundException;

    public void changeDeviceValue(String deviceId, String valueId, Value value) throws NotFoundException;
}
