package de.oss4home.bc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.oss4home.bc.boundary.RcDataService;
import de.oss4home.bc.boundary.RcService;
import de.oss4home.bc.dto.RcState;
import de.oss4home.bc.entity.Device;
import de.oss4home.swagger.generate.api.NotFoundException;
import de.oss4home.swagger.generate.model.Devices;
import de.oss4home.swagger.generate.model.Devicetype;
import de.oss4home.swagger.generate.model.Devicetype.ValueEnum;
import de.oss4home.swagger.generate.model.UpdateDevice;
import de.oss4home.swagger.generate.model.Value;
import de.oss4home.swagger.generate.model.Value.ValuetypeEnum;
import de.oss4home.swagger.generate.model.Values;

@Service
public class DeviceControllerImpl implements DevicesController {

    @Autowired
    RcDataService rcDataService;

    @Autowired
    RcService rcService;

    @Override
    public void changeDeviceSettings(String deviceId, UpdateDevice setting) throws NotFoundException {

    }

    @Override
    public de.oss4home.swagger.generate.model.Device getDevices(String deviceId) throws NotFoundException {
	Device currentDevice = rcDataService.getDeviceById(Integer.parseInt(deviceId));
	de.oss4home.swagger.generate.model.Device device = new de.oss4home.swagger.generate.model.Device();
	device.setId("" + currentDevice.getId());
	device.setValues(getDeviceValues(device.getId()));
	setDeviceSettings(device);
	return device;
    }

    @Override
    public Value getDeviceValue(String deviceId, String valueId) throws NotFoundException {
	if (valueId.equals("1")) {
	    return getDeviceValues(deviceId).get(0);
	} else {
	    throw new NotFoundException(1, "");
	}
    }

    @Override
    public Values getDeviceValues(String deviceId) throws NotFoundException {
	Values values = new Values();
	Device device = rcDataService.getDeviceById(Integer.parseInt(deviceId));
	if (device != null) {
	    Value value = new Value();
	    value.setId(1);
	    value.setReadonly(false);
	    value.setValuetype(ValuetypeEnum.booleanValue);
	    value.setValue(device.getValue() == RcState.ON ? "true" : "false");
	    values.add(value);
	    return values;
	} else {
	    throw new NotFoundException(1, "");
	}
    }

    @Override
    public Devices getDevices(String devicetype, String sytemtype) throws NotFoundException {
	List<Device> devices = rcDataService.getDevices();
	Devices resultDevices = new Devices();
	for (Device currentDevice : devices) {
	    de.oss4home.swagger.generate.model.Device device = new de.oss4home.swagger.generate.model.Device();
	    device.setId("" + currentDevice.getId());
	    device.setValues(getDeviceValues(device.getId()));
	    setDeviceSettings(device);
	    resultDevices.add(device);
	}
	return resultDevices;

    }

    private void setDeviceSettings(de.oss4home.swagger.generate.model.Device device) {
	Devicetype typ = new Devicetype();
	typ.setValue(ValueEnum.switchBinary);
	device.setDevicetype(typ);
	device.getPossibledevicetypes().add(typ);
    }

    @Override
    public void changeDeviceValue(String deviceId, String valueId, Value value) throws NotFoundException {
	Device device = rcDataService.getDeviceById(Integer.parseInt(deviceId));

	if (valueId.equals("1") && device != null
		&& (value.getValue().equals("true") || value.getValue().equals("false"))) {
	    RcState rcvalue;
	    if (value.getValue().equals("true")) {
		rcvalue = RcState.ON;
	    } else {
		rcvalue = RcState.OFF;
	    }
	    rcService.send(device.getSystemCode(), device.getDeviceAdress(), rcvalue);
	    // rcService.send(rcvalue == RcState.ON ? device.getOnCode() :
	    // device.getOffCode());
	    device.setValue(rcvalue);

	} else {
	    throw new NotFoundException(1, "");
	}
    }

}
