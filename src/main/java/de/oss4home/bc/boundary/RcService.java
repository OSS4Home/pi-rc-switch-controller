package de.oss4home.bc.boundary;

import de.oss4home.bc.dto.RcDeviceAdress;
import de.oss4home.bc.dto.RcState;

public interface RcService {
    void send(String groupid, RcDeviceAdress deviceid, RcState state);

}
