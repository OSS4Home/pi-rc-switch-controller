package de.oss4home.bc.boundary;

import java.util.BitSet;

import org.springframework.stereotype.Service;

import com.pi4j.io.gpio.RaspiPin;

import de.oss4home.bc.dto.RcDeviceAdress;
import de.oss4home.bc.dto.RcState;
import de.pi3g.pi.rcswitch.RCSwitch;

@Service
public class RcServiceImpl implements RcService {

    RCSwitch transmitter;

    public RcServiceImpl() {
	transmitter = new RCSwitch(RaspiPin.GPIO_00);
    }

    @Override
    public void send(String groupid, RcDeviceAdress deviceid, RcState state) {
	try {
	    BitSet address = RCSwitch.getSwitchGroupAddress(groupid);

	    if (state == RcState.ON) {
		transmitter.switchOn(address, deviceid.getValue());
	    } else {
		transmitter.switchOff(address, deviceid.getValue());
	    }
	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }
}