package de.oss4home.bc.boundary;

import java.util.List;

import de.oss4home.bc.entity.Device;

public interface RcDataService {
    void initData();

    List<Device> getDevices();

    Device getDeviceById(int id);

}
