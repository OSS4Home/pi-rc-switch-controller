package de.oss4home.bc.boundary;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import de.oss4home.bc.dto.RcDeviceAdress;
import de.oss4home.bc.dto.RcState;
import de.oss4home.bc.entity.Device;

@Service
public class RcDataServiceImpl implements RcDataService {

    private List<Device> devices = new ArrayList<>();

    public RcDataServiceImpl() {
	initData();
    }

    @Override
    public void initData() {
	devices.add(new Device(1, RcState.OFF, "01110", RcDeviceAdress.A));
	devices.add(new Device(2, RcState.OFF, "01110", RcDeviceAdress.B));
	devices.add(new Device(3, RcState.OFF, "01110", RcDeviceAdress.C));
	devices.add(new Device(4, RcState.OFF, "01110", RcDeviceAdress.D));

    }

    @Override
    public List<Device> getDevices() {
	return devices;

    }

    @Override
    public Device getDeviceById(int id) {
	for (Device device : devices) {
	    if (device.getId() == id) {
		return device;
	    }

	}
	return null;
    }

}
