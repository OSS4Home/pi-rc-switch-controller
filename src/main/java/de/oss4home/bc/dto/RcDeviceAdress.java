package de.oss4home.bc.dto;

public enum RcDeviceAdress {
    A(1), B(2), C(3), D(4);
    public final int value;

    private RcDeviceAdress(int value) {
	this.value = value;
    }

    public int getValue() {
	return this.value;
    }

}
