package de.oss4home.bc.entity;

import de.oss4home.bc.dto.RcDeviceAdress;
import de.oss4home.bc.dto.RcState;

public class Device {
    private int id;
    private RcState value;
    private String systemCode;
    private RcDeviceAdress deviceAdress;

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public RcState getValue() {
	return value;
    }

    public void setValue(RcState value) {
	this.value = value;
    }

    public String getSystemCode() {
	return systemCode;
    }

    public void setSystemCode(String systemCode) {
	this.systemCode = systemCode;
    }

    public RcDeviceAdress getDeviceAdress() {
	return deviceAdress;
    }

    public void setDeviceAdress(RcDeviceAdress deviceAdress) {
	this.deviceAdress = deviceAdress;
    }

    public Device(int id, RcState value, String systemCode, RcDeviceAdress deviceAdress) {
	super();
	this.id = id;
	this.value = value;
	this.systemCode = systemCode;
	this.deviceAdress = deviceAdress;

    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((deviceAdress == null) ? 0 : deviceAdress.hashCode());
	result = prime * result + id;
	result = prime * result + ((systemCode == null) ? 0 : systemCode.hashCode());
	result = prime * result + ((value == null) ? 0 : value.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Device other = (Device) obj;
	if (deviceAdress != other.deviceAdress)
	    return false;
	if (id != other.id)
	    return false;
	if (systemCode == null) {
	    if (other.systemCode != null)
		return false;
	} else if (!systemCode.equals(other.systemCode))
	    return false;
	if (value != other.value)
	    return false;
	return true;
    }

    @Override
    public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append("Device [id=").append(id).append(", value=").append(value).append(", systemCode=")
		.append(systemCode).append(", deviceAdress=").append(deviceAdress).append("]");
	return builder.toString();
    }

    // private String convertRcBrennnStuhlToSendAdress(String groupAdress,
    // RcDeviceAdress deviceAdress,
    // RcState deviceState) {
    // String converterAdress = "";
    //
    // for (char charakter : groupAdress.toCharArray()) {
    // if (charakter == '1') {
    // converterAdress += "00";
    // } else if (charakter == '0') {
    // converterAdress += "01";
    // } else {
    // throw new IllegalArgumentException();
    // }
    //
    // }
    //
    // String[] codeDevice = new String[] { "0001010101", "0100010101",
    // "0101000101", "0101010001", "0101010100" };
    // converterAdress += codeDevice[deviceAdress.value - 1];
    // if (deviceState == RcState.ON) {
    // converterAdress += "0001";
    // } else {
    // converterAdress += "0100";
    // }
    // return "" + Integer.parseInt(converterAdress, 2);
    // }

}
